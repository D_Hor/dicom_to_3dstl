License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 
02110-1301 USA.

# DICOM_to_3DSTL
Algorithm implemented in Python to create printable, 3D STL model from CT images (cross-section).  Algorithm uses Blender 2.78 to performs model postprocessing.

version 1.0.0. June 1, 2017

## Requirements
All required Python modules and third-party applications are listed below:  
- Python 3 (https://www.python.org/)
- Python modules (scripts have been tested using the following releases of modules):  
	-- numpy: 1.13.0  
	-- scipy: 0.19.1  
	-- natsort: 5.0.3  
	-- pydicom: 0.9.9  
	-- numpy-stl: 2.2.3  
- Blender 2.78 (https://www.blender.org/)

## Algorithm structure
The algorithm consist of 5 modules:  
1. _1DICOMAnonymizer.py  
2. _2Sorting.py  
3. _3Preprocessing.py  
4. _4MeshCreate.py  
5. _5PostprocessingSTL.py  

## Usage
An example of a 3D STL model reconstruction from CT images

### Important
Before running please ensure that the following the conditions are met:
- The algorithm works only on CT cross-section. A folder with CT images must contain cross-section images, but may also contain a different type of DICOM images (only cross-sections will be used).
- All scripts must be in single directory
- Script names have not been changed
- There are no spaces between scans, scans overlap, otherwise, there will be gaps between layers in the 3d model. This is crucial for proper 3D reconstruction
- Spatial resolution (voxel size) should be high, for example 0.5mm, 1mm, 2mm (the algorithm was tested for voxel size 0.39x0.39x0.63mm)

### How to use
1) Open *STL_creating.py* script
2) Specify a path to CT DICOM scans - *dicomPath*
3) Specify a path to the new folder for saving preprocessed images - *savePath*
4) Specify a path to the Blender 2.78 software (the path must include the Blender.exe) - *Blender_path*
5) Set a threshold value in the Hounsfield units for bone segmentation - *threshold_HU*
6) Set a volume threshold in voxels (by default *volume_type=0*) to filter small structures - *threshold_volume* (to set the volume threshold in mm3 set the parameter *volume_type=1*)
7) Optionally - one can anonymize yours dicom files by uncomment the following code line: 
    *#dicomPath = _1DICOMAnonymizer.anonymization(dicomPath, savePath)*
8) Run this script

### How it works
After running the *STL_creating.py* script the DICOM files are first sorted/set in the correct position using the *_2Sorting.py* script.  
(optionally: files are first anonimized using the *_1DICOMAnonymizer.py* script and then sorted/set in the correct position using the *_2Sorting.py* script
Next, images are segmented and filtered using thresholdHUs and threshold values parameters respectivelly, using the *_3Preprocessing.py* script.  
Then, the 3D STL model is reconstructed using the *_4MeshCreate.py* script, that uses images preprocessed in the previous step.  
After successful reconstruction, the model is automatically postprocessed in the Blender software. At the beginning, Blender is opened automatically without using the user interface (in console mode) and next the *_5PostprocessingSTL.py* script is executed (also automatically). During post-processing, the geometry of the model is cleaned and the model is smoothed.
After finishing the model processing, the final STL model is saved in a new folder in the same directory as preprocessed images (*savePath*). The 3D model is also saved in *.blend* format.

### Warning
- The algorithm is not optimized. The reconstruction time can be long.
- For the recnstruction of big objects like the skull at least 8GB RAM memory is required.

# Example CT data
In Example_CT_data one can find a sample set of CT images of the skull