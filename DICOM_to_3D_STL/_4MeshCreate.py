'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section). Algorithm uses Blender 2.78 to perform model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:
- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py


IV. MeshCreate.py


Copyright 2017 Dominik Horwat, Marek Krośnicki


License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:

The function creates 3D model, plane by plane, through creating mesh from 2D CT DICOM images

1) Get the paths of preprocessed images
2) Calculate the new thickness of the voxel
3) Create a reference cube (voxel) by defining the coordinates of vertices (width, height and thickness of voxel) and faces
4) Create mesh (plane by plane) from cubes with simultaneous deleting internal faces based on neighborhood analysis
5) Create a 3D model from previously created planes
6) Saving the 3D model in .stl format


Syntax:

[stl_path] = create_mesh(_PathDicom, ArrayDicom, PixelCoord, PixelSpacing, PixelDims, direct_cos)


Input:

dicomPath - path to a new folder containing preprocessed DICOM files (newdicomPath from thresholding())

ArrayDicom - a 3D pixel array of preprocessed images (newArrayDicom from thresholding())

PixelCoord - a 3D array storing coordinates of each image plane (PixelCoord from thresholding())

PixelSpacing - a vector of physical dimensions of voxel (PixelSpacing from thresholding())

PixelDims - a vector of image size (in pixels) and files number (PixelDims from thresholding())

direct_cos - a vector containing direction cosines of images plane (direct_cos from thresholding())


Output:

stl_path - path to the folder containing the created 3D model

3D stl model saved in the stl_path

'''

from stl import mesh
import dicom as pydicom
import os
import numpy as np
import copy

def create_mesh(dicomPath, ArrayDicom, PixelCoord, PixelSpacing, PixelDims, direct_cos):

    #Create a new folder to saving the 3D model
    stl_path = dicomPath + '_Model'
    if not os.path.exists(stl_path):
        os.makedirs(stl_path)

    #Load reference spacing values (in mm)
    VoxelSpacing = (PixelSpacing[0][0], PixelSpacing[0][1], PixelSpacing[0][2])

    #Get the width and height of the image (in pixels)
    width = PixelDims[0]
    height = PixelDims[1]

    #Calculate the new voxel thickness
    if(abs(PixelCoord[0][2] - PixelCoord[1][2]) < VoxelSpacing[2]):
        z_spacing = VoxelSpacing[2] - (VoxelSpacing[2] - abs(abs(PixelCoord[0][2])-abs(PixelCoord[1][2])))
    else:
        z_spacing = VoxelSpacing[2] + abs(PixelCoord[2] - PixelCoord[2])
    

    #Define the 8 vertices of the cube
    vertices2 = np.array([\
        [PixelCoord[0][0] - VoxelSpacing[0]/2, PixelCoord[0][1] - VoxelSpacing[1]/2, -z_spacing/2],
        [PixelCoord[0][0] + VoxelSpacing[0]/2, PixelCoord[0][1] - VoxelSpacing[1]/2, -z_spacing/2],
        [PixelCoord[0][0] + VoxelSpacing[0]/2, PixelCoord[0][1] + VoxelSpacing[1]/2, -z_spacing/2],
        [PixelCoord[0][0] - VoxelSpacing[0]/2, PixelCoord[0][1] + VoxelSpacing[1]/2, -z_spacing/2],
        [PixelCoord[0][0] - VoxelSpacing[0]/2, PixelCoord[0][1] - VoxelSpacing[1]/2, z_spacing/2],
        [PixelCoord[0][0] + VoxelSpacing[0]/2, PixelCoord[0][1] - VoxelSpacing[1]/2, z_spacing/2],
        [PixelCoord[0][0] + VoxelSpacing[0]/2, PixelCoord[0][1] + VoxelSpacing[1]/2, z_spacing/2],
        [PixelCoord[0][0] - VoxelSpacing[0]/2, PixelCoord[0][1] + VoxelSpacing[1]/2, z_spacing/2]
        ])

    #Define the triangles faces composing the cube
    faces = np.array([\
        #-z
        [0,3,1],
        [1,3,2],
        #-x    
        [0,4,7],
        [0,7,3],
        #+z
        [4,5,6],
        [4,6,7],
        #+x
        [5,1,2],
        [5,2,6],
        #+y
        [2,3,6],
        [3,7,6],
        #-y
        [0,1,5],
        [0,5,4]])

    #Create the reference cube
    #Create a mesh
    cube = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))

    #Fill the mesh 
    for i, f in enumerate(faces):
        for j in range(3):
            cube.vectors[i][j] = vertices2[f[j],:]
    
    cubes2 = []

    #Flip an array in the up direction
    ArrayDicomtemp = np.flipud(ArrayDicom)
    size = PixelSpacing.shape[0]
    #Creating the mesh
    #Loop over all images (planes)

    for i in range(0,size):

        #Load spacing values (in mm)
        VoxelSpacing = (PixelSpacing[i][0], PixelSpacing[i][1], PixelSpacing[i][2])

        #Create a list of empty cubes 
        cubes=[]

        img_size = np.nonzero(ArrayDicomtemp[:,:,i])
        
        if(img_size[0].min() == 0):
            size_x1 = img_size[0].min() + 1 
        else:
            size_x1 = img_size[0].min()

        if(img_size[0].max() == (PixelDims[0] - 1)):
            size_x2 = img_size[0].max()
        else:
            size_x2 = img_size[0].max() + 1

        if(img_size[1].min() == 0):
            size_y1 = img_size[1].min() + 1 
        else:
            size_y1 = img_size[1].min()

        if(img_size[1].max() == (PixelDims[1] - 1)):
            size_y2 = img_size[1].max()
        else:
            size_y2 = img_size[1].max() + 1
        
        #Loop over all pixels
        for x in range(size_x1, size_x2): 
                for y in range(size_y1, size_y2): 
                    #Check the threshold value 
                    if (ArrayDicomtemp[x,y,i] > 0):
                        
                        #Copy the reference cube - creating a new cube
                        temp_cube = mesh.Mesh(cube.data.copy())

                        #Cleaning the internal faces
                        if(ArrayDicomtemp[x+1,y,i] > 0):
                            temp_cube.vectors[6] = 0
                            temp_cube.vectors[7] = 0
                        
                        if(ArrayDicomtemp[x,y+1,i] > 0):
                            temp_cube.vectors[8] = 0
                            temp_cube.vectors[9] = 0

                        if(ArrayDicomtemp[x-1,y,i] > 0):
                            temp_cube.vectors[2] = 0
                            temp_cube.vectors[3] = 0

                        if(ArrayDicomtemp[x,y-1,i] > 0):
                            temp_cube.vectors[10] = 0
                            temp_cube.vectors[11] = 0

                        #Check if this is the first or last plane
                        if(i < size-1):
                            if(ArrayDicomtemp[x,y,i+1] > 0):
                                temp_cube.vectors[4] = 0
                                temp_cube.vectors[5] = 0

                        if(i > 0):
                            if(ArrayDicomtemp[x,y,i-1] > 0):
                                temp_cube.vectors[0] = 0
                                temp_cube.vectors[1] = 0

                        #Create a filtered mesh
                        temp_cube = mesh.Mesh(temp_cube.data, remove_empty_areas = True)

                        #Translate the new object in relation to coordinates and direction cosines
                        temp_cube.translate([VoxelSpacing[0]*x*direct_cos[0],VoxelSpacing[1]*y*direct_cos[4],PixelCoord[i][2]])

                        #Add the cube to list                    
                        cubes2.extend(temp_cube.data)
        print("Slice: ",i+1)
        
    print("Saving STL..")
    #Create a 3D object from cubes
    plane = mesh.Mesh(np.concatenate([cubes2],axis=0))

    #Save the object as an .stl file
    plane.save(stl_path+'/'+'Model_STL.stl')
    print("The STL model was successfully saved in: " + stl_path)
    return stl_path

