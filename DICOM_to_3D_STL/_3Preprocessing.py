'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section). Algorithm uses Blender 2.78 to perform model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:
- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py


III. Preprocessing.py


Copyright 2017 Dominik Horwat, Marek Krośnicki


License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:

The function preprocessing CT images loaded from DICOM files

1) loading and collecting images (pixel arrays) and information about images
2) converting the grayscale to the Hounsfield scale 
3) thresholding images using a choosen threshold value (in the Hounsfield scale)
4) re-converting images to the grayscale
5) performing a 3D neighborhood analysis and a 3D connected components labeling to find and separate connected objects
6) carrying out a 3D filtering procedure using a chosen maximum volume threshold
7) saving preprocessed images as new files in the new folder


Syntax:

[newdicomPath, newArrayDicom, PixelCoord, PixelSpacing, PixelDims, direct_cos] = thresholding(dicomPath, threshold_HUs, threshold_volume)


Input:

dicomPath - path to a folder containing DICOM files (from load_PathDicom function)

threshold_HUs - a threshold value in Hounsfield units

threshold_volume -  the volume in voxels (or mm3) below which all objects should be removed



Output:

newdicomPath - path to a new folder containing preprocessed DICOM files

newArrayDicom - a 3D pixel array of preprocessed images  

PixelCoord - a 3D array storing coordinates of each image plane 

PixelSpacing - a vector of voxel physical dimensions

PixelDims - a vector of image size - rows and columns (in pixels) and files number

direct_cos - a vector containing direction cosines of images plane

'''

import dicom as pydicom
import os
import numpy as np
from natsort import natsorted, ns
from scipy.stats import threshold
from scipy import ndimage
from os.path import join


def preprocessing(dicomPath, threshold_HUs, threshold_volume, volume_type=0):

    temp = []
    lstFilesDCM = []
    
    for dirName, subdirList, fileList in os.walk(dicomPath):
        for filename in fileList:
            temp.append(os.path.join(dirName,filename))

    lstFilesDCM = natsorted(temp)

    #Load a reference file
    RefDs = pydicom.read_file(lstFilesDCM[0],force=True)
    #Load dimensions based on the number of rows, columns, and slices (along the Z axis)
    PixelDims = (int(RefDs.Rows), int(RefDs.Columns), len(lstFilesDCM))

    #Get the direction cosine
    direct_cos = RefDs.ImageOrientationPatient

    #Get the Rescale parameters
    slope = RefDs.RescaleSlope
    intercept = RefDs.RescaleIntercept

    #Create arrays (the ArrayDicom is sized based on 'PixelDims')
    ArrayDicom = np.zeros(PixelDims, dtype = RefDs.pixel_array.dtype)
    PixelCoord = np.zeros([len(lstFilesDCM),3])
    PixelSpacing = np.zeros([len(lstFilesDCM),3])

    #Create the new folder to saving preprocessed images
    newdicomPath = dicomPath + '_preproc'
    if not os.path.exists(newdicomPath):
        os.makedirs(newdicomPath)

    i=0
    print("Segmentation...")
    #Load all DICOM images (pixel arrays)
    for filenameDCM in lstFilesDCM:
        #Read the file
        ds = pydicom.read_file(filenameDCM, force=True)
        #Store the raw image data
        ArrayDicom[:, :, lstFilesDCM.index(filenameDCM)] = ds.pixel_array
        #Store the each image plane coordinates
        PixelCoord[i, :] = ds.ImagePositionPatient
        #Store the voxel dimensions
        PixelSpacing[i, :] = (float(ds.PixelSpacing[0]), float(ds.PixelSpacing[1]), float(RefDs.SliceThickness))

        i=i+1

    array_type = RefDs.pixel_array.dtype.name    
    #Converting the grayscale to the Hounsfield scale
    ArrayDicom = slope*ArrayDicom-abs(intercept)

    #Check the number of allocated bits and set the binary value
    if (array_type == 'uint16'):
        binary = 65534
    if(array_type == 'int16'):
        binary = 32766
    if (array_type == 'uint8'):
        binary = 255
    if(array_type == 'int8'):
        binary = 126
    
    #Thresholding images
    thresholded = threshold(ArrayDicom, threshold_HUs)
    ArrayDicom = thresholded

    #Re-converting the Hounsfield units to the grayscale
    ArrayDicom[ArrayDicom!=0] += abs(intercept)

    #Convert the array to uint16
    ArrayDicom = np.uint16(ArrayDicom)
    
    #Images binarization 
    indArrayDicom = ArrayDicom > 0
    ArrayDicom[indArrayDicom] = binary

    #Set the 0 value to pixels at the edges of images - the zero-padding
    ArrayDicom[0:PixelDims[0],0] = 0
    ArrayDicom[0:PixelDims[0],PixelDims[1]-1] = 0
    ArrayDicom[0,0:PixelDims[1]] = 0
    ArrayDicom[PixelDims[0]-1,0:PixelDims[1]] = 0

    #The array that defines feature connections
    s = np.array([[[1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]],

   [[1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]],

   [[1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]]], dtype='uint32')

    #Labeling features in an images array 
    labels = ndimage.measurements.label(ArrayDicom, structure = s)

    labels = labels[0]
    
    newArrayDicom = np.zeros(ArrayDicom.shape, ArrayDicom.dtype)

    #Convert the array to uint32
    newArrayDicom = np.uint32(labels)
    
    #Find objects in the labeled array
    objects = ndimage.find_objects(newArrayDicom)

    #Calculate the voxel volume
    voxel_volume = PixelSpacing[0][0] * PixelSpacing[0][1] * PixelSpacing[0][2]
    
    j=0

    print("Filtering...")
    #Initialize a filtering procedure
    while(j<len(objects)):

        #Find nonzero indexs in the j object
        nonzero_index = newArrayDicom[objects[j]].nonzero()
        
        #Create an array containing only nonzero pixels values
        temp = newArrayDicom[objects[j]][nonzero_index]

        maxim = j+1
        #Evaluate the number of pixels represented by the j+1 label value 
        voxel_amount = (temp == maxim).sum()

        if(volume_type == 0):
            #Clean the mesh by deleting small structures
            if (voxel_amount < threshold_volume):
                #Set 0 if the structure is small enough to delete (deleting object)
                temp[temp == maxim] = 0
            else:
                #Set the nonzero value if the structure is big enough to not being deleted
                temp[temp == maxim] = 65000

        elif(volume_type == 1):
            voulme_mm3 = voxel_amount*voxel_volume
            if(voulme_mm3 < threshold_volume):
                #Set 0 if the structure is small enough to delete (deleting object)
                temp[temp == maxim] = 0
            else:
                #Set the nonzero value if the structure is big enough to not being deleted
                temp[temp == maxim] = 65000
        else:
            raise ValueError('Please specify volume_type=0 or volume_type=1 (see Readme file)')
        
        #Set new values in images array instead of the j+1 label value(filtering)
        newArrayDicom[objects[j]][nonzero_index] = temp
        j=j+1

    #Convert the array to uint16
    newArrayDicom = np.uint16(newArrayDicom)
    
    #Save new DICOM files
    print("Saving files...")
    i=0
    for filenameDCM in lstFilesDCM:
        #Read the file
        ds = pydicom.read_file(filenameDCM,force=True)
        ds.pixel_array = newArrayDicom[:,:,i]
        ds.pixel_array = ds.pixel_array.astype(array_type)
        ds.PixelData = newArrayDicom[:,:,i].tostring()
        ds.save_as(newdicomPath + "/"+str(i)+".dcm")
        i=i+1
    print("Files were successfully saved in: " + newdicomPath)
    

    import gc
    gc.collect()
    return newdicomPath, newArrayDicom, PixelCoord, PixelSpacing, PixelDims, direct_cos
