'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section).
Algorithm uses Blender 2.78 to perform model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:
- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py

Sorting.py

Copyright 2017 Dominik Horwat, Marek Krośnicki

License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:

The function sorts CT images loaded from DICOM files

1) Loading and collecting images (pixel arrays) and information about images
2) Separating cross-section images and sorting these images according to the z coordinate
3) Saving images of each group in a proper folder (separation cross-sections, sagittal etc.)

Input:

dicomPath - path to a folder containing the DICOM files

savePath - path to save sorted DICOM files 


Output:

The new folder with sorted DICOM files

'''

from operator import itemgetter
import dicom as pydicom
import os
import numpy
from natsort import natsorted, ns
import sys

def sorting(dicomPath, savePath):

    #Creating a new folder
    if not os.path.exists(savePath):
        os.makedirs(savePath)

    temp = []
    lstFilesDCM = []

    #Adding all DICOM files path to a list
    for dirName, subdirList, fileList in os.walk(dicomPath):
        for filename in fileList:
            temp.append(os.path.join(dirName,filename))

    lstFilesDCM = natsorted(temp)

    i=0
    j=0
    arr = {}

    while(j < len(lstFilesDCM)):
        #Load the DICOM file
        ds = pydicom.read_file(lstFilesDCM[j], force = True)

        #Get only cross-section images
        try:
            if(ds.ImageType[0] == 'ORIGINAL' and ds.ImageType[2]== 'AXIAL'):
                arr[i] = (ds.ImagePositionPatient[2], lstFilesDCM[j])
                i = i + 1

        except AttributeError:
            continue
        j = j + 1

    #Sorting images according to the z coordinate
    sort = sorted(arr.items(), key=itemgetter(1))

    lstFilesDCM.clear()

    #Create a new list with sorted DICOM files
    for i in range(len(sort)):
        lstFilesDCM.append(sort[i][1][1])
    #Saving files
    for i in range(len(sort)):
        ds = pydicom.read_file(lstFilesDCM[i], force = True)
        ds.save_as(savePath + "/img_" + str(i))
        print("Saving file: ",i)

    print("Files were successfully saved in: " + savePath)
    os.startfile(savePath)
    import gc
    gc.collect()
